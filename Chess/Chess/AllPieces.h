#pragma once
#include "ChessPiece.h"
#include "Bishop.h"
#include "Rook.h"
#include "Pawn.h"
#include "King.h"
#include "Queen.h"
#include "Knight.h"