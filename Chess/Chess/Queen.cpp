#include "Queen.h"

Queen::Queen(Point& place, bool side) : StraightPiece(place, side ? 'Q' : 'q', side)
{
}

bool Queen::justChecking(const Point& dst, ChessPiece* board[][BOARD_SIZE])
{
	return StraightPiece::checkMovement(dst, board) || StraightPiece::checkExtraMove(dst, board);
}

bool Queen::boardCheckMove(const Point& dst, ChessPiece* board[][BOARD_SIZE])
{
	if (StraightPiece::checkMovement(dst, board) || StraightPiece::checkExtraMove(dst, board))
	{
		this->_place = dst;
		return true;
	}
	return false;
}