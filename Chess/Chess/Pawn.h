#pragma once
#include "ChessPiece.h"

#define LINE_PAWN_WHITE 6
#define LINE_PAWN_BLACK 1

class Pawn : public ChessPiece
{
public:
	Pawn(Point& place, bool side);
	virtual bool checkMovement(const Point& dst, ChessPiece* board[][BOARD_SIZE]);
	bool rested();
};