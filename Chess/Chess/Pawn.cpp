#include "Pawn.h"

Pawn::Pawn(Point& place, bool side) : ChessPiece(place, side ? 'P' : 'p', side)
{
}
// need some fixing still
bool Pawn::checkMovement(const Point& dst, ChessPiece* board[][BOARD_SIZE])
{
	bool fresh = false;
	int range = this->rested() ? 2:1;
	range = this->_side ? range : -range;
	int foward = this->_place.getY() - dst.getY(); // change if need opposite.
	int sideways = abs(this->_place.getX() - dst.getX());

	if (board[dst.getY()][dst.getX()]) // eat
	{
		return (this->_side ? foward == 1 : foward == -1) && sideways == 1;
	}
	if (this->rested())
	{
		fresh = foward == range - (range / abs(range));
	}
	return (!(foward - range) && !sideways) || fresh;
}

bool Pawn::rested()
{
	return this->_place.getY() == LINE_PAWN_BLACK || this->_place.getY() == LINE_PAWN_WHITE;
}