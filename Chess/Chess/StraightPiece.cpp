#include "StraightPiece.h"

StraightPiece::StraightPiece(Point& place, char type, bool side) : ChessPiece(place, type, side)
{
}

bool StraightPiece::checkMovement(const Point& dst, ChessPiece* board[][BOARD_SIZE])
{// + straight
	int lenX = this->_place.getX() - dst.getX();
	int lenY = this->_place.getY() - dst.getY();
	int sum = lenY + lenX;

	if (sum == lenX || sum == lenY) // Can get there.
	{
		int jumpX = 0;
		int jumpY = 0;
		int x = this->_place.getX();
		int y = this->_place.getY();

		lenY ? jumpY = lenY / abs(lenY) : jumpX = lenX / abs(lenX);

		for (int i = 1; i < abs(lenX + lenY); i++)
		{
			x -= jumpX;
			y -= jumpY;
			if (board[y][x]) // there's a piece there.
			{
				return false;
			}
		}
	}
	else
	{
		return false;
	}
	return true;
}

bool StraightPiece::checkExtraMove(const Point& dst, ChessPiece* board[][BOARD_SIZE])
{// x cross
	int lenX = this->_place.getX() - dst.getX();
	int lenY = this->_place.getY() - dst.getY();

	if (!lenX || !lenY)
	{
		return false;
	}

	if (!(abs(lenX) == abs(lenY))) // Can get there.
	{
		return false;
	}

	int jumpX = lenX / abs(lenX);
	int jumpY = lenY / abs(lenY);
	int x = this->_place.getX();
	int y = this->_place.getY();

	for (int i = 1; i < abs(lenY); i++)
	{
		x -= jumpX;
		y -= jumpY;
		if (board[y][x]) // there's a piece there.
		{
			return false;
		}
	}

	return true;
}