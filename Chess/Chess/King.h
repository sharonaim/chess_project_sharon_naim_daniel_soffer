#pragma once
#include "ChessPiece.h"
#include "Rook.h"

#define KING_RANGE 1.5 // Root 2. but it's ok like that
#define ROOK_POS1 0
#define ROOK_POS2 7

class King : public ChessPiece
{
public:
	King(Point& place, bool side);
	virtual bool checkMovement(const Point& dst, ChessPiece* board[][BOARD_SIZE]);
private:
	bool _fresh;
	static bool checkRook(const Point& dst, ChessPiece* board[][BOARD_SIZE]);
};