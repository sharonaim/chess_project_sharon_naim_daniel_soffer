#pragma once
#include <iostream>
#include "Pipe.h"
#include "SocketException.h"
#include <thread>
#include <exception>
#include "Board.h"

#define START_PIECE  "rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR0"
#define MSG_SIZE 1024 
#define SOCKET_MSG_LEN 4
#define QUIT_MSG "quit"

using namespace std;

class Game
{

public:
	Game();
	void gameRun();
	string printReconnect();
	void checkMsgFromSocket();
	void sendToPiece();
	Point changeToPoint(char ch, char num);

private:
	Board _arena;
	bool _turn;
	bool _isRunning;
	//Point _src;
	//Point _dst;
	char _msgToGraphics[MSG_SIZE];
	string _msgFromGraphics;
	string _msgFromChessPiece;
};