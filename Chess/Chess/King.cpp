#include "King.h"

King::King(Point& place, bool side) :  ChessPiece(place, side ? 'K' : 'k', side)
{
	this->_fresh = true;
}

bool King::checkMovement(const Point& dst, ChessPiece* board[][BOARD_SIZE])
{
	double dist = distance(this->_place, dst);

	// Castling.
	/*if (this->_fresh && this->checkRook(dst, board))
	{
		int jumpX = this->_place.getX() - dst.getX() / abs(this->_place.getX() - dst.getX());
		int jumpY = this->_place.getY() - dst.getY() / abs(this->_place.getY() - dst.getY());
		ChessPiece* dstPiece = board[dst.getY()][dst.getX()];

		if (dstPiece->isFresh() && dstPiece->getSide() == this->_side)
		{
			int len = this->_place.getX() - dst.getX();
			for (int i = 1; i < len; i++)
			{
				if (board[this->_place.getY()][this->_place.getX() + (i * jumpX)])
				{
					return false; // cant castling
				}
			}
			// switch places.
			return true;
		}
	} */


	if (KING_RANGE > dist)
	{
		this->_place = dst;
		this->_fresh = false;
		return true;
	}
	return false;
}

bool King::checkRook(const Point& dst, ChessPiece* board[][BOARD_SIZE])
{
	bool type = board[dst.getY()][dst.getX()]->getType();
	return type == 'k' || type == 'K';
}