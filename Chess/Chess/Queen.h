#pragma once
#include "StraightPiece.h"

class Queen : public StraightPiece
{
public:
	Queen(Point& place, bool side);
	virtual bool justChecking(const Point& dst, ChessPiece* board[][BOARD_SIZE]);
	virtual bool boardCheckMove(const Point& dst, ChessPiece* board[][BOARD_SIZE]);
};